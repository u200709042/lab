public class MyDate {

    int day, month, year;

    int[] maxDays = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    public MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public String toString() {
        return year + "-" + (month < 10 ? "0" + month : month) + "-" + (day < 10 ? "0" + day : day);
    }

    public void incrementDay() {
        day++;
        if (day > maxDays[month - 1]) {
            day = 1;
            incrementMonth();
        } else if (month == 2 && day == 29 && !inLeapYear()) {
            day = 1;
            incrementMonth();
        }
    }

    public boolean inLeapYear() {
        return year % 4 == 0;
    }


    public void incrementYear(int diff) {
        year += diff;
        if (month == 2 && day == 29 && !inLeapYear()) {
            day = 28;
        }
    }

    public void decrementDay() {
        day--;
        if (day == 0) {
            decrementMonth();
            if (!inLeapYear() && month == 2) {
                day = 28;
            } else {
                day = maxDays[month - 1];
            }
        }
    }

    public void decrementYear() {
        incrementYear(-1);
    }

    public void decrementMonth() {
        decrementMonth(1);
    }

    public void incrementDay(int diff) {
        while (diff > 0) {
            incrementDay();
            diff--;
        }
    }

    public void decrementMonth(int diff) {
        incrementMonth(-diff);
    }

    public void incrementMonth(int diff) {
        month += diff;
        int yearDiff = (month - 1)/ 12;
        int newMonth = ((month - 1) % 12) + 1;

        if (newMonth < 0)
            yearDiff--;
        year += yearDiff;

        month = newMonth < 0 ? newMonth + 12 : newMonth;

        if (day > maxDays[month - 1]) {
            day = maxDays[month - 1];
            if (month == 2 && day == 29 && !inLeapYear())
                day = 28;
        }
    }

    public void decrementDay(int diff){
        while (diff > 0) {
            decrementDay();
            diff--;
        }
    }

    public void decrementYear(int diff) {
        incrementYear(-diff);
    }

    public void incrementMonth(){
        incrementMonth(1);
    }

    public void incrementYear(){
        incrementYear(1);
    }


    public boolean isBefore(MyDate anotherDate) {
        boolean x = false;
        if(year < anotherDate.year)
            x = true;
        else if(month < anotherDate.month)
            x = true;
        else if(day < anotherDate.day)
            x = true;

        return x;
        }

    public boolean isAfter(MyDate anotherDate) {
        boolean x = false;
        if(year > anotherDate.year)
            x = true;
        else if(month > anotherDate.month)
            x = true;
        else if(day > anotherDate.day)
            x = true;

        return x;
    }

    public int dayDifference(MyDate anotherDate) {
        int diff = 0;
        if(isBefore(anotherDate)) {
            MyDate dateX = new MyDate(day, month, year);
            while (dateX.isBefore(anotherDate)) {
                dateX.incrementDay();
                diff++;
            }
        }
        else if(isAfter(anotherDate)){
            MyDate dateX = new MyDate(day, month, year);
            while (dateX.isAfter(anotherDate)) {
                dateX.decrementDay();
                diff++;
            }
        }
        return diff;
    }
}
