package drawing.version3;

import java.util.ArrayList;

import shapes.Circle;
import shapes.Rectangle;
import shapes.Shape;
import shapes.Square;

public class Drawing {
	
	private ArrayList<Shape> shapes = new ArrayList<Shape>();
	
	public double calculateTotalArea(){
		double totalArea = 0;

		for (Shape shape : shapes) {
			totalArea += shape.area();

		}
		return totalArea;
	}
	public void addShape(Shape shape) {
		shapes.add(shape);
	}
}


// version 2'da shape dışındaki object kabul ediyor (integer veya string gibi), ama bunda shape dışında herhangi bir object kabul edilmiyor